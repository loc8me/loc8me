﻿function ShowAjaxLoader() {
	if ($("#AjaxLoader").length == 0) {
        $("body").append('<div id="AjaxLoader" style="display: none"><div><span></span></div></div>');
    }
    $("#AjaxLoader").css("display", "block");
}

function HideAjaxLoader() {
    $("#AjaxLoader").css("display", "none");
}

$(document).ready(function(){
	window.onbeforeunload = function(event) {
		ShowAjaxLoader();
	};
	
	//SetSmallFooter();
	//$( window ).resize(function(){
	//	SetSmallFooter();
	//});
	//$.datepicker.setDefaults( $.datepicker.regional[ "it" ] );	
});

function SetSmallFooter(){
	$("#body").css("min-height","");
	var footerTop=$("footer").offset().top;
	var winHeight=$(window).height();
	if(footerTop<winHeight){
		var bdHeight=$("#body").height();
		bdHeight=bdHeight+(winHeight-footerTop)-40;
		$("#body").css("min-height",bdHeight+"px");
	}
	
}

function UpdateTableHeader(table) {
    var maxlength = [];
    $("tbody tr:first td", table).each(function (i) {
        maxlength[i] = $(this).width();
    });
    if (maxlength.length == 0) {
        return;
    }
    $("thead tr:first td", table).each(function (i) {
        $(this).css("display", "inlne-block")
        $(this).css("max-width", maxlength[i] + "px");
        $(this).css("width", maxlength[i] + "px");
    });

}