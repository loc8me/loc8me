﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Loc8me.Default" %>

<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <title>Loc8Me - Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/owl.carousel.css">
    <link rel="stylesheet" href="../css/owl.theme.css">
    <link rel="stylesheet" href="../css/styles.css">
    <script src="../js/modernizr.custom.32033.js"></script>
    <!--[if IE]><script type="text/javascript" src="js/excanvas.compiled.js"></script><![endif]-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .slider-item-size {
            width: 300px;
            height: 500px;
        }

        .snap-item-size {
            width: 300px;
            height: 500px;
            padding-left: 10px;
        }
    </style>
</head>
<body>
    <form runat="server">
    <div class="pre-loader">
        <div class="load-con">
            <img src="../img/logo.png" class="animated fadeInDown" alt="">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>
    <!-- Wrap all page content here -->
    <div id="wrap">
        <header class="masthead">
            <div class="slider-container" id="slider">
                <div class="container">
                    <div class="row mh-container">
                        <h1>Loc8Me ( Beta Version )</h1>
                        <h3>New Location Tracking Mobile Application</h3>
                        <div class="col-md-4 col-md-push-4">
                            <div class="btn-group btn-group-justified btn-lg small">

                                <div class="btn-group">
                                    <a href="#" class="btn btn-default scrollpoint sp-effect6">
                                        <span class="android"></span>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-10 col-md-push-1 hidden-xs mh-slider">
                            <div class="row">
                                <div class="col-md-3" runat="server">
                                    <a href="Downloads/Loc8me.apk" class="btn btn-default side">Download now!</a>
                                    <%--<span style="cursor:pointer" class="downloadfile btn btn-default scrollpoint sp-effect1">download now!</span>--%>
                                </div>
                                <div class="col-md-6">
                                    <div id="carousel-slider" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <img src="../img/slide1.png" class="slider-item-size" alt="..." class="img-responsive">
                                            </div>

                                            <div class="item">
                                                <img src="../img/slide2.png" class="slider-item-size" alt="..." class="img-responsive">
                                            </div>
                                            <div class="item">
                                                <img src="../img/slide3.png" class="slider-item-size" alt="..." class="img-responsive">
                                            </div>
                                            <div class="item">
                                                <img src="../img/slide4.png" class="slider-item-size" alt="..." class="img-responsive">
                                            </div>
                                            <div class="item">
                                                <img src="../img/slide5.png" class="slider-item-size" alt="..." class="img-responsive">
                                            </div>

                                        </div>
                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-slider" role="button" data-slide="prev">
                                            <span class="slider-left"></span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-slider" role="button" data-slide="next">
                                            <span class="slider-right"></span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-3">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fixed navbar -->
            <div class="navbar navbar-static-top" id="nav" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <i class="fa fa-align-justify"></i>
                        </button>
                        <a class="navbar-brand" href="#">
                            <img src="../img/logo.png" alt="">
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav social hidden-xs hidden-sm">
                            <li><a href="#"><i class="fa fa-google-plus fa-lg fa-fw"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter fa-lg fa-fw"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook fa-lg fa-fw"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest fa-lg fa-fw"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin fa-lg fa-fw"></i></a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#slider">Home</a></li>
                            <li><a href="#features">features</a></li>
                            <li><a href="#download">download</a></li>
                            <li><a href="#subscribe">subscribe!</a></li>
                            <li><a href="#contact">contact</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!--/.container -->
            </div>
            <!--/.navbar -->

        </header>
        <section id="features">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-push-2 clearfix">
                        <div class="section-heading scrollpoint sp-effect3">
                            <h3>Loc8Me<span> Application features</span></h3>
                            <span class="divider"></span>
                            <p>A multi purpose, highly flexible application which has many potential uses for different markets and scenarios</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect2">
                                                <div class="icon">
                                                    <i class="fa fa-star fa-4x"></i>
                                                </div>
                                                <h4>No Limit</h4>
                                                <p>With this mobile application, you can trace locations of as many people as you want. You can find their current locations, you can see where they are moving.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect2">
                                                <div class="icon">
                                                    <i class="fa fa-lock fa-4x"></i>
                                                </div>
                                                <h4>Data Privacy</h4>
                                                <p>Using this mobile application, you will have full control on your data. Your data is your data, it will be in your own hands and will be in your own control. You are the only person who can access this data.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect1">
                                                <div class="icon">
                                                    <i class="fa fa-dollar fa-4x"></i>
                                                </div>
                                                <h4>Free App Support</h4>
                                                <p>We will provide you free support with dedicated support staff to solve any upcoming issues.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect1">
                                                <div class="icon">
                                                    <i class="fa fa-cogs fa-4x"></i>
                                                </div>
                                                <h4>Easy Settings</h4>
                                                <p>We offer on demand customization for this mobile application. You can get in this mobile application what you want. You can get customization according to your organization.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect2">
                                                <div class="icon">
                                                    <i class="fa fa-refresh fa-4x"></i>
                                                </div>
                                                <h4>Continous Improvement</h4>
                                                <p>We provide you complete support to improve the application features according to your requirement</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect2">
                                                <div class="icon">
                                                    <i class="fa fa-download fa-4x"></i>
                                                </div>
                                                <h4>How to download</h4>
                                                <p>This app can be downloaded on this webiste. Bur in future, for specific organization aftar customizing, it can only be downloaded from their server.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect1">
                                                <div class="icon">
                                                    <i class="fa fa-eye fa-4x"></i>
                                                </div>
                                                <h4>Security</h4>
                                                <p>With this mobile application you are getting best data security as your data is not in cloud and not with third party. It is in your hands. Noboday can not find where are your sales men.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="feature scrollpoint sp-effect1">
                                                <div class="icon">
                                                    <i class="fa fa-wrench fa-4x"></i>
                                                </div>
                                                <h4>Change your app according to your needs</h4>
                                                <p>This app is highly customizable. Every thing can be changed on demand, you can select the background themes and desired look and feel. Everything, is in your hands, and in your access..</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="download">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-push-1">
                        <h1>Mobile Application <span>Loc8Me</span> Download</h1>
                        <h4>This Application can be downloded here !</h4>
                    </div>
                    <div class="col-md-6 col-md-push-3">
                        <div class="btn-group btn-group-justified btn-lg small">
                            <div class="btn-group">

                            </div>
                            <div class="btn-group">

                            </div>
                            <div class="btn-group">
                                <a href="#" class="btn btn-default scrollpoint sp-effect6">
                                    <span class="android"></span>
                                </a>
                            </div>
                            <div class="btn-group">

                            </div>
                        </div>
                    </div>

                    <div class="btn-group btn-group-justified btn-lg">
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="row">

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="row">
                                    <div class="btn-group scrollpoint sp-effect4">
                                        <a href="#" class="btn btn-default">
                                            <span class="playstore"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="settings">
            <div class="container">
                <div class="col-md-7">
                    <img src="../img/loc8me.png" alt="" class="">
                    <h2 class="scrollpoint sp-effect3">Loc8Me <span>Application Settings</span></h2>
                    <p class="first">This application can easily be installed and configured for your mobile phone. There are few simple steps that need to be followed to complete setup!</p>
                    <p>
                        First of all you will download the application and then register your account. After registering, you can create your profile with your picture. You can search other users and send them request.
                        If they accept your request, you can trace them. You can also block any user at any time
                    </p>
                    <p>
                         <a href="Downloads/Loc8me.apk" class="btn btn-default side">Download now!</a>
                        <%--<span style="cursor:pointer" class="downloadfile btn btn-default scrollpoint sp-effect1">download now!</span>--%>
                        <a href="#" class="btn btn-empty scrollpoint sp-effect2">learn more</a>
                    </p>
                </div>
                <div class="col-md-5">
                    <img src="../img/login.png" class="img-responsive hidden-xs iphone-settings" alt="">
                </div>
            </div>
        </section>
        <section id="screenshots">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 hidden-xs">
                        <span class="owl-prev"><i class="fa fa-chevron-left fa-2x"></i></span>
                    </div>
                    <div class="col-md-8">
                        <div class="section-heading scrollpoint sp-effect3">
                            <h3>Loc8Me<span> screen shots</span></h3>
                            <span class="divider"></span>
                            <p>To understand the functionlity, check the below screen shots</p>
                        </div>
                    </div>
                    <div class="col-md-2 hidden-xs">
                        <span class="owl-next"><i class="fa fa-chevron-right fa-2x"></i></span>
                    </div>
                </div>
            </div>
            <div id="owl-screenshots" class="owl-carousel">
                <div><img class="snap-item-size" src="../img/screenshot1.png" alt=""></div>
                <div><img class="snap-item-size" src="../img/Screenshot2.png" alt=""></div>
                <div><img class="snap-item-size" src="../img/Screenshot3.png" alt=""></div>
                <div><img class="snap-item-size" src="../img/Screenshot4.png" alt=""></div>
                <div><img class="snap-item-size" src="../img/Screenshot5.png" alt=""></div>
                <div><img class="snap-item-size" src="../img/Screenshot6.png" alt=""></div>
                <div><img class="snap-item-size" src="../img/screenshot1.png" alt=""></div>
            </div>
        </section>

        <section id="packages">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-push-2 clearfix">
                        <div class="section-heading scrollpoint sp-effect3">
                            <h3>Loc8Me<span> member packed ( coming soon )</span></h3>
                            <span class="divider"></span>
                            <p>This is price information of different Loc8Me packages</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="pack scrollpoint sp-effect6">
                            <div class="heading">
                                <h3>Free</h3>
                                <h1>?<sup>$</sup></h1>
                                <h5>monthly</h5>
                            </div>
                            <div class="details">
                                <ul>
                                    <li>Website</li>
                                    <li>User</li>
                                    <li>GB Storage</li>
                                    <li>GB Bandwidth</li>
                                    <li>Support Ticket System</li>
                                </ul>
                                <a href="#" class="btn btn-sign-up btn-block">Sign now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pack scrollpoint sp-effect6">
                            <div class="heading">
                                <h3>Premium</h3>
                                <h1>?<sup>$</sup></h1>
                                <h5>monthly</h5>
                            </div>
                            <div class="details">
                                <ul>
                                    <li>Website</li>
                                    <li>User</li>
                                    <li>GB Storage</li>
                                    <li>GB Bandwidth</li>
                                    <li>Support Ticket System</li>
                                </ul>
                                <a href="#" class="btn btn-sign-up btn-block">Sign now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pack scrollpoint sp-effect6">
                            <div class="heading">
                                <h3>Aluminium</h3>
                                <h1>?<sup>$</sup></h1>
                                <h5>monthly</h5>
                            </div>
                            <div class="details">
                                <ul>
                                    <li>Website</li>
                                    <li>User</li>
                                    <li>GB Storage</li>
                                    <li>GB Bandwidth</li>
                                    <li>Support Ticket System</li>
                                </ul>
                                <a href="#" class="btn btn-sign-up btn-block">Sign now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="pack scrollpoint sp-effect6">
                            <div class="heading">
                                <h3>Helium</h3>
                                <h1>?<sup>$</sup></h1>
                                <h5>monthly</h5>
                            </div>
                            <div class="details">
                                <ul>
                                    <li>Website</li>
                                    <li>User</li>
                                    <li>GB Storage</li>
                                    <li>GB Bandwidth</li>
                                    <li>Support Ticket System</li>
                                </ul>
                                <a href="#" class="btn btn-sign-up btn-block">Sign now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="testimonials">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-push-1">
                        <div id="carousel-testimonials" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="../img/akbar.jpg" alt="">
                                    <div class="carousel-caption">
                                        <h3>"Loc8Me is great and a big support for our business as we can find the location of other persons while marketing and sales."</h3>
                                        <h2>Mr. Akbar - Business - Lahore</h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="../img/taha.jpg" alt="">
                                    <div class="carousel-caption">
                                        <h3>"Loc8Me is great that always helps my parents to trace me and my location. They always want to know where i am, now Loc8Me made is possible for them"</h3>
                                        <h2>Mr. Taha - Student - Lahore</h2>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="../img/imtiaz.jpg" alt="">
                                    <div class="carousel-caption">
                                        <h3>
                                            "I have a big line of transport vehicles whithin city. Lastly i do not know where are my vehicles and drivers. Not because of Loc8Me, i have complete information.
                                            Where they are and where they going. A really nice impact on business."
                                        </h3>
                                        <h2>Mr. Imtiaz - Transporter - Karachi</h2>
                                    </div>
                                </div>
                            </div>
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-testimonials" data-slide-to="1"></li>
                                <li data-target="#carousel-testimonials" data-slide-to="2"></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<section id="team">
            <div class="container">
                <div id="carousel-team" class="carousel slide" data-ride="carousel">
                    <div class="row">
                        <div class="col-md-2 hidden-xs">
                            <a class="car-prev" href="#carousel-team" role="button" data-slide="prev">
                                <i class="fa fa-chevron-left fa-2x"></i>
                            </a>
                        </div>
                        <div class="col-md-8">
                            <div class="section-heading scrollpoint sp-effect3">
                                <h3>Loc8Me<span> team members</span></h3>
                                <span class="divider"></span>
                                <p>"Talent wins games, but teamwork and intelligence wins championships. Michael Jordan"</p>
                            </div>
                        </div>
                        <div class="col-md-2 hidden-xs">
                            <a class="car-next" href="#carousel-team" role="button" data-slide="next">
                                <i class="fa fa-chevron-right fa-2x"></i>
                            </a>
                        </div>
                    </div>

                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="member">
                                            <div class="media">
                                                <a class="pull-left" href="#">
                                                    <img class="media-object" src="../img/ceo.jpg" alt="">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        Media heading
                                                    </h4>
                                                    <p>Founder &amp; CEO</p>
                                                    <span class="divider"></span>
                                                    <p>Being a Project Manager is like being an artist, you have the different colored process streams combining into a work of art.</p>

                                                </div>
                                            </div>
                                        </div>
                                         <div class="member">
                                            <div class="media">
                                                <a class="pull-left" href="#">
                                                    <img class="media-object" src="../img/MMan.jpg" alt="">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        Media heading

                                                    </h4>
                                                    <p>Marketing</p>
                                                    <span class="divider"></span>
                                                    <p>
                                                        Word of mouth is the most valuable form of marketing, but you can't buy it. You can only deliver it.
                                                        And you have to really deliver.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="member">
                                            <div class="media">
                                                <a class="pull-left" href="#">
                                                    <img class="media-object" src="../img/TCON.jpg" alt="">
                                                </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading">
                                                        Media heading

                                                    </h4>
                                                    <p>Technical Consultant</p>
                                                    <span class="divider"></span>
                                                    <p>Good design adds value faster than it adds cost. You can't have great software without a great team, and most software teams behave like dysfunctional families.</p>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

   <footer id="contact">
            <div class="container">
                <div class="row">
                    
                    
                    <div class="col-md-12">
                        <div class="social">
                            <ul>
                                
                                
                                <li><a href="#"><i class="fa fa-facebook fa-lg"></i></a></li>
                                
                            </ul>
                        </div>
                        <p class="rights">
                            <span>Loc8Me</span> Landing Page Developed by <span>Team Loc8me</span>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
        <div id="DownloadHidden" style="display:none"></div>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/waypoints.min.js"></script>

    <!-- jQuery REVOLUTION Slider  -->
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="../js/script.js"></script>
        <script src="script/GenericLib.js"></script>
        <script src="script/DownloadFile.js"></script>
        <script src="script/custom.js"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
        </form>
</body>
</html>