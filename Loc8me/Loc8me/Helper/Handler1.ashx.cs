﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loc8me.Helper
{
    /// <summary>
    /// Summary description for Handler1
    /// </summary>
    public class Handler1 : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            
            context.Response.ContentType = "application/octet-stream";
            context.Response.AppendHeader("Content-Disposition", "attachment; filename=Loc8me.apk");
            context.Response.TransmitFile(context.Server.MapPath("../Downloads/Loc8me.apk"));
            context.Response.End();
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}