﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loc8me.WebServices.Entities
{
    public class CoordinateDetails
    {
        public int Aid;
        public String Latitude { get; set; }
        public String Longitude { get; set; }
        public String DateTime { get; set; }


    }   
}