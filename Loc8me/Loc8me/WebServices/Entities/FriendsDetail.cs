﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loc8me.WebServices.Entities
{
    public class FriendsDetail
    {
        public int AssetId { get; set; }
        public String Name { get; set; }
        public String Gender { get; set; }
        public String PhoneNumber { get; set; }
        public String UserName { get; set; }
        public String ImageUrl { get; set; }
        public String LastLat { get; set; } = "0";
        public String LastLng { get; set; } = "0";
        public String LastLocationTime { get; set; } = "0";
        public String Type { get; set; } = "0";
        public int FriendId { get; set; }
        public int FriendRequestId { get; set; }

    }
}