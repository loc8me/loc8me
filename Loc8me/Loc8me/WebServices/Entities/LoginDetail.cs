﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loc8me.WebServices.Entities
{
    public class LoginDetail
    {
        public int Uid{ get; set; }
        public String Name { get; set; } = "N/A";
        public String ImageUrl { get; set; } = "N/A";
        public String Gender { get; set; } = "N/A";
        public String PhoneNumber { get; set; } = "N/A";
        public String Status { get; set; } = "N/A";
        public String Type { get; set; } = "0";
    }
}