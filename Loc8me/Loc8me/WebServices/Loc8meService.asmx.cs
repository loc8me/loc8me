﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using Loc8me.WebServices.Entities;
using System.Web.Security;
using System.IO;
using Loc8me.AppCode;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace Loc8me.WebServices
{
    /// <summary>
    /// Summary description for Loc8meService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Loc8meService : System.Web.Services.WebService
    {
      // DateTime dateTime=DateTime.Now.AddHours(5);
        DateTime dateTime = DateTime.Now;


        //string hostUrl = HttpContext.Current.Request.Url.Host;
       string hostUrl = "http://loc8m.com";

        [WebMethod]
        public LoginDetail SignIn(String UserName, String Password)
        {
            LoginDetail loginDetail = new LoginDetail();
            
            try
            {
                SQLHelper helper = new SQLHelper();
                String encrpytedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");
                SqlParameter usernameParam = new SqlParameter("@UserName", UserName);
                SqlParameter passwordParam = new SqlParameter("@Password", encrpytedPassword);
                var dt = helper.ExecuteDataTable(CommandType.Text, "Select * from asset where UserName=@UserName and Password=@Password", usernameParam, passwordParam);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var asset = dt.Rows[0];
                    if (!(bool)asset["Block"])
                    {
                        loginDetail.Uid = (int)asset["Asset_Id"];
                        loginDetail.Name = asset["AssetName"].ToString();
                        loginDetail.Gender = asset["Gender"].ToString();
                        loginDetail.ImageUrl = asset["Image"].ToString();
                        loginDetail.PhoneNumber = asset["PhoneNumber"].ToString();
                        loginDetail.Type = asset["Type"].ToString();
                        loginDetail.Status = "sucess";
                    }
                    else
                    {
                        loginDetail.Status = "Blocked";
                    }
                }
                else
                {
                    loginDetail.Status = "Invalid Credational";
                }
            }
            catch (Exception ex)
            {
                loginDetail.Status = ex.ToString();
            }
            return loginDetail;
        }
        [WebMethod]
        public String SignUp(String UserName, String Password, String PhoneNumber, String CompanyCode)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                SqlParameter codeParam = new SqlParameter("@Code", CompanyCode);
                var dt = helper.ExecuteDataTable(CommandType.Text, "Select * from Company where Code=@Code", codeParam);
                if (!(dt.Rows.Count > 0))
                {
                    return "invalid code";
                }
                else
                {
                    var company = dt.Rows[0];
                    int companyId = (int)company["Company_Id"];
                    SqlParameter companyIdParam = new SqlParameter("@Company_Id", companyId);
                    SqlParameter userNameParam = new SqlParameter("@UserName", UserName);

                    if (helper.ExecuteDataTable(CommandType.Text, "Select * from asset where Company_Id=@Company_Id and UserName=@UserName", companyIdParam, userNameParam).Rows.Count > 0)
                        return "already register";
                    else
                    {
                        String encrpytedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");
                        string addAssetQry = "Insert into Asset(UserName,PhoneNumber,Block,Password,Company_Id) Values(@UserName,@PhoneNumber,0,@Password,@Company_Id)";
                        SqlParameter phoneNumberParam = new SqlParameter("@PhoneNumber", PhoneNumber);
                        SqlParameter passwordParam = new SqlParameter("@Password", encrpytedPassword);
                        helper.ExecuteNonQuery(CommandType.Text, addAssetQry, userNameParam, phoneNumberParam, passwordParam, companyIdParam);
                        String assetId = helper.ExecuteScalar(CommandType.Text, "Select Asset_Id from asset where UserName=@UserName", userNameParam).ToString();
                        return assetId;
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        [WebMethod]
        public LoginDetail SavePersonalInfo(int AssetId, String AssetName, String Gender, String Type, String Image)
        {
            try
            {
                    SQLHelper helper = new SQLHelper();
                    String Imageurl;
                   
                   // hostUrl = "http://loc8me.azurewebsites.net";
                    if (Image != null && !Image.Equals(""))
                    {
                        var myUniqueFileName = string.Format(@"{0}.png", Guid.NewGuid());
                        SaveImage(Image, myUniqueFileName);
                        Imageurl = hostUrl + "/images/" + myUniqueFileName;
                    }
                    else
                    {
                        Imageurl = hostUrl + "/images/default.png";
                    }
                    String savePersonalInfoQry = "Update asset set AssetName=@AssetName,Gender=@Gender,Type=@Type,Image=@Image where Asset_Id=@AssetId";
                    SqlParameter assetNameParam = new SqlParameter("@AssetName", AssetName);
                    SqlParameter genderParam = new SqlParameter("@Gender", Gender);
                    SqlParameter typeParam = new SqlParameter("@Type", Type);
                    SqlParameter imageParam = new SqlParameter("@Image", Imageurl);
                    SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                    helper.ExecuteNonQuery(CommandType.Text, savePersonalInfoQry, assetNameParam, genderParam, typeParam, imageParam, assetIdParam);

             
                    var loginDetail = new LoginDetail
                    {
                        Uid = AssetId,
                        Status = "sucess",
                        Name = AssetName,
                        Gender = Gender,
                        PhoneNumber = helper.ExecuteScalar(CommandType.Text, "Select PhoneNumber from asset where Asset_Id=@AssetId", assetIdParam).ToString(),
                        ImageUrl = Imageurl,
                        Type = Type
                    };
                    return loginDetail;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        [WebMethod]
        public List<CoordinateDetails> getAssetsLocation(int AssetId, List<UserAids> UserAidList)
        //public List<CoordinateDetails> getAssetsLocation()
        {
            //    int AssetId = 144;
            //    List<UserAids> UserAidList = new List<UserAids>();
            //    UserAids l1 = new UserAids() { Aid = 144 };
            //    UserAids l2 = new UserAids() { Aid = 147};
            //    //UserAids l3 = new UserAids() { Aid = 93 };
            //    UserAidList.Add(l1); UserAidList.Add(l2);

            SQLHelper helper = new SQLHelper();
            DataTable dt = new DataTable();
            StringBuilder Qry = new StringBuilder(String.Empty);
            List<int> _idsList = new List<int>();

            List<CoordinateDetails> coordinatesDetail = new List<CoordinateDetails>();
            try
            {
                Qry.Append("Select Whom_Id from friend where Of_Id=@AssetId and Whom_Id in(" + GetCommaSepartedStringFromList(UserAidList.Select(x => x.Aid).ToList()) + ") and Block=0;");
                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                dt = helper.ExecuteDataTable(CommandType.Text, Qry.ToString(), assetIdParam);
                foreach (DataRow row in dt.Rows)
                {
                    _idsList.Add((int)row["Whom_Id"]);
                }
                Qry = new StringBuilder(String.Empty);
                dt = new DataTable();
                Qry.Append("Select * from AssetLocation a where a.Asset_Id in (" + GetCommaSepartedStringFromList(_idsList) + ") and a.AssetLocation_Id=");
                Qry.Append("(Select max(AssetLocation_Id) from AssetLocation where Asset_Id = a.Asset_Id group by Asset_Id)");
                dt = helper.ExecuteDataTable(CommandType.Text, Qry.ToString());

                foreach (DataRow row in dt.Rows)
                {
                    coordinatesDetail.Add(new CoordinateDetails
                    {
                        Aid = (int)row["Asset_Id"],
                        Latitude = row["Latitude"].ToString(),
                        Longitude = row["Longitude"].ToString(),
                        DateTime = ((DateTime)row["DateTime"]).ToString("MM /dd/yyyy HH:mm:ss")
                    });
                }
                return coordinatesDetail;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [WebMethod]
        public String SendFriendRequest(int FromId, int ToId)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                SqlParameter fromIdParam = new SqlParameter("@FromId", FromId);
                SqlParameter toIdParam = new SqlParameter("@ToId", ToId);
                helper.ExecuteNonQuery(CommandType.Text, "Insert into FriendRequest (From_Id,To_Id,DateTime) Values (@FromId,@ToId,getDate())", fromIdParam, toIdParam);
                return "sucess";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [WebMethod]
        public String AcceptFriendRequest(int WhomId, int OfId)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                SqlParameter whomIdParam = new SqlParameter("@WhomId", WhomId);
                SqlParameter ofIdParam = new SqlParameter("@OfId", OfId);

                helper.ExecuteNonQuery(CommandType.Text, "Delete FriendRequest where From_Id = @OfId and To_Id = @WhomId;", whomIdParam, ofIdParam);
                helper.ExecuteNonQuery(CommandType.Text, "Insert into Friend (Whom_Id,Of_Id,Block,DateTime) Values(@WhomId,@OfId,0,getDate())", whomIdParam, ofIdParam);

                return "sucess";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [WebMethod]
        public String BlockFriend(int OfId, int WhomId)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                SqlParameter whomIdParam = new SqlParameter("@WhomId", WhomId);
                SqlParameter ofIdParam = new SqlParameter("@OfId", OfId);
                helper.ExecuteNonQuery(CommandType.Text, "Update Friend set Block=1 where Whom_Id = @WhomId and Of_Id = @OfId;", whomIdParam, ofIdParam);
                return "sucess";

            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [WebMethod]
        public String UnBlockFriend(int OfId, int WhomId)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                SqlParameter whomIdParam = new SqlParameter("@WhomId", WhomId);
                SqlParameter ofIdParam = new SqlParameter("@OfId", OfId);
                helper.ExecuteNonQuery(CommandType.Text, "Update Friend set Block=0 where Whom_Id = @WhomId and Of_Id = @OfId;", whomIdParam, ofIdParam);

                return "sucess";            
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [WebMethod]
        public String SendLocation(int AssetId, String Latitude, String Longitude)
        {
            try
            {
                SQLHelper helper = new SQLHelper();

                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                SqlParameter latitudeParam = new SqlParameter("@Latitude", Convert.ToDouble(Latitude));
                SqlParameter longitudeParam = new SqlParameter("@Longitude", Convert.ToDouble(Longitude));
                helper.ExecuteNonQuery(CommandType.StoredProcedure, "UpdateLocation", assetIdParam, latitudeParam, longitudeParam);
                return "sucess";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
      
        [WebMethod]
        public List<FriendsDetail> getTrackingMeFriendsList(int AssetId)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                DataTable dt = new DataTable();
                StringBuilder Qry = new StringBuilder(String.Empty);
                List<FriendsDetail> friendsDetail = new List<FriendsDetail>();

                Qry.Append("Select a.Asset_Id,a.AssetName,a.Gender,a.PhoneNumber,a.UserName,a.Image,a.Type ");
                Qry.Append("from Asset a  join Friend f on a.Asset_Id = f.Of_Id ");
                Qry.Append("where f.Whom_Id = @AssetId");

                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                dt = helper.ExecuteDataTable(CommandType.Text, Qry.ToString(), assetIdParam);

                foreach (DataRow row in dt.Rows)
                {
                    FriendsDetail friendDetail = new FriendsDetail
                    {
                        AssetId = (int)row["Asset_Id"],
                        Name = row["AssetName"].ToString(),
                        Gender = row["Gender"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString(),
                        UserName = row["UserName"].ToString(),
                        ImageUrl = row["Image"].ToString(),
                        Type = row["Type"].ToString(),
                    };
                    friendsDetail.Add(friendDetail);
                }
                return friendsDetail;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [WebMethod]
        public List<FriendsDetail> getTrackingToFriendsList(int AssetId, int FriendId)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                DataTable dt = new DataTable();
                StringBuilder Qry = new StringBuilder(String.Empty);
                List<FriendsDetail> friendsDetail = new List<FriendsDetail>();
                Qry.Append("Select a.Asset_Id,a.AssetName,a.Gender,a.PhoneNumber,a.UserName,a.Image,a.Type,f.Friend_Id ");
                Qry.Append("from Asset a join Friend f on a.Asset_Id=f.Whom_Id ");
                Qry.Append("where f.Of_Id = @AssetId and f.Friend_Id> @FriendId");

                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                SqlParameter friendIdParam = new SqlParameter("@FriendId", FriendId);

                dt = helper.ExecuteDataTable(CommandType.Text, Qry.ToString(), assetIdParam, friendIdParam);

                foreach (DataRow row in dt.Rows)
                {

                    FriendsDetail friendDetail = new FriendsDetail
                    {
                        AssetId = (int)row["Asset_Id"],
                        Name = row["AssetName"].ToString(),
                        Gender = row["Gender"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString(),
                        UserName = row["UserName"].ToString(),
                        ImageUrl = row["Image"].ToString(),
                        Type = row["Type"].ToString(),
                        FriendId = (int)row["Friend_Id"],
                    };
                    friendsDetail.Add(friendDetail);
                }

                for (int i = 0; i < friendsDetail.Count; i++)
                {
                    int _id = friendsDetail[i].AssetId;
                    dt = new DataTable();
                    Qry = new StringBuilder(String.Empty);
                    Qry.Append("Select Top 1 * from AssetLocation where Asset_Id=@AssetId order by AssetLocation_Id desc");

                    dt = helper.ExecuteDataTable(CommandType.Text, Qry.ToString(), assetIdParam);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        var row = dt.Rows[0];
                        friendsDetail[i].LastLat = row["Latitude"].ToString();
                        friendsDetail[i].LastLng = row["Longitude"].ToString();
                        friendsDetail[i].LastLocationTime = ((DateTime)row["DateTime"]).ToString("MM /dd/yyyy HH:mm:ss");
                    }                 
                }
                return friendsDetail;
            }
            catch (Exception ex)
            {
                return null;
                //   return ex.ToString();
            }
        }

        [WebMethod]
        public List<FriendsDetail> getFriendRequestList(int AssetId,int Id)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                DataTable dt = new DataTable();
                StringBuilder Qry = new StringBuilder(String.Empty);
                List<FriendsDetail> friendsDetailList = new List<FriendsDetail>();

                Qry.Append("Select asset.Asset_Id,asset.AssetName,asset.Gender,asset.PhoneNumber,asset.UserName,asset.Image,asset.Type,friendRequest.FriendRequest_Id ");
                Qry.Append("from Asset asset join  FriendRequest friendRequest on asset.Asset_Id=friendRequest.From_Id ");
                Qry.Append("where friendRequest.To_Id=@AssetId and friendRequest.FriendRequest_Id>@Id");

                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                SqlParameter idParam = new SqlParameter("@Id", Id);

                dt = helper.ExecuteDataTable(CommandType.Text, Qry.ToString(), assetIdParam, idParam);

                foreach (DataRow row in dt.Rows)
                {
                    FriendsDetail friendsDetail = new FriendsDetail
                    {
                        AssetId = (int)row["Asset_Id"],
                        Name = row["AssetName"].ToString(),
                        Gender = row["Gender"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString(),
                        UserName = row["UserName"].ToString(),
                        ImageUrl = row["Image"].ToString(),
                        Type = row["Type"].ToString(),
                        LastLat = "0",
                        LastLng = "0",
                        LastLocationTime = "0",
                        FriendRequestId = (int)row["FriendRequest_Id"]
                    };
                    friendsDetailList.Add(friendsDetail);
                }
                return friendsDetailList;
            }
            catch (Exception)
            {
                return null;
            }
        }

         [WebMethod]
        public List<FriendsDetail> getSearchUserList(int AssetId,string UserName)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                DataTable dt = new DataTable();
                List<FriendsDetail> friendsDetailList = new List<FriendsDetail>();

                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                var companyId = (int)helper.ExecuteScalar(CommandType.Text, "Select Company_Id from asset where Asset_Id=@AssetId", assetIdParam);
                SqlParameter companyIdParam = new SqlParameter("@CompanyId", companyId);
                SqlParameter userNameParam = new SqlParameter("@UserName","%"+ UserName+"%");

                String Qry = "Select * from asset where Company_Id=@CompanyId and Asset_Id!=@AssetId and (Type = '1' or Type='3') and UserName like @UserName";
                dt = helper.ExecuteDataTable(CommandType.Text, Qry, companyIdParam, assetIdParam, userNameParam);

                if (dt == null || dt.Rows.Count == 0)
                {
                    Qry = "Select * from asset where Company_Id=@CompanyId and Asset_Id!=@AssetId and (Type = '1' or Type='3') and AssetName like @UserName";
                    dt = helper.ExecuteDataTable(CommandType.Text, Qry, companyIdParam, assetIdParam, userNameParam);
                }

                foreach (DataRow row in dt.Rows)
                {
                    FriendsDetail friendsDetail = new FriendsDetail
                    {
                        AssetId = (int)row["Asset_Id"],
                        Name = row["AssetName"].ToString(),
                        Gender = row["Gender"].ToString(),
                        PhoneNumber = row["PhoneNumber"].ToString(),
                        UserName = row["UserName"].ToString(),
                        ImageUrl = row["Image"].ToString()
                    };
                    friendsDetailList.Add(friendsDetail);
                }
                return friendsDetailList;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [WebMethod]
        public String UpdateImage(int AssetId, String Image)
        {
            try
            {
                String Imageurl;
                if (Image != null)
                {
                    var myUniqueFileName = string.Format(@"{0}.png", Guid.NewGuid());
                    SaveImage(Image, myUniqueFileName);
                    Imageurl = hostUrl + "/images/" + myUniqueFileName;
                }
                else
                {
                    Imageurl = hostUrl + "/images/default.png";
                }
                SQLHelper helper = new SQLHelper();
                String updateImageQry = "Update asset set Image=@Image where Asset_Id=@AssetId";
                SqlParameter imageUrlParam = new SqlParameter("@Image", Image);
                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);
                helper.ExecuteNonQuery(CommandType.Text, updateImageQry, imageUrlParam, assetIdParam);

                return Imageurl;
            }
            catch (Exception)
            {
                return null;
            }

        }

        [WebMethod]
        public String UpdatePersonalInfo(int AssetId, String AssetName, String Gender)
        {
            try
            {
                SQLHelper helper = new SQLHelper();
                String updatePersonalInfoQry = "Update asset set AssetName=@AssetName,Gender=@Gender where Asset_Id=@AssetId";
                SqlParameter assetnameParam = new SqlParameter("@AssetName", AssetName);
                SqlParameter genderParam = new SqlParameter("@Gender", Gender);
                SqlParameter assetIdParam = new SqlParameter("@AssetId", AssetId);

                helper.ExecuteNonQuery(CommandType.Text, updatePersonalInfoQry, assetnameParam, genderParam, assetIdParam);
                    return "sucess";
            }
            catch (Exception e)
            {
                return null;
            }
        }
        private void SaveImage(string base64, String Name)
        {

       byte[] imageBytes = Convert.FromBase64String(base64);

            string uploadPath = Server.MapPath("..\\Images\\") + Name;
            using (FileStream fs = File.Create(uploadPath))
            using (BinaryWriter bw = new BinaryWriter(fs))
                bw.Write(imageBytes);                
        }

        private String GetCommaSepartedStringFromList(List<int> list)
        {
            StringBuilder sb = new StringBuilder(String.Empty);
            foreach(int item in list)
            {
                sb.Append(item + ",");
            }
            String str = sb.ToString();
           return str.ToString().Remove(str.Length - 1);
        }
     }
}
